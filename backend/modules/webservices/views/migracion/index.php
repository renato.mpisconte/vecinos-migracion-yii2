<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TablasMigracionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tablas Migracion';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(
    '@web/assets/backend/js/migracion.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="tabla-migracion-index">
            <h1><?= Html::encode($this->title) ?></h1>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    'id_tbl',
                    'nom_tbl',
                    [
		        'attribute' => 'estado_tbl',
		        'value' => function($model) {
                            $estado = ($model->estado_tbl == '1')?'Migrado':'Pendiente';
                            return $estado;
                        }//,
//                        'contentOptions'=>['class'=>'btn btn-success text-center']
                    ],
                    'fecha_registro',
                     'descripcion',
                     ['class' => 'yii\grid\ActionColumn',
                        'template'=>'{procesar}',
                        'buttons'=>[
                            'procesar' => function ($url, $model) {
                                $nom_tbl = $model['nom_tbl'];
                                return Html::button('<span class="glyphicon glyphicon-wrench"> <b>Procesar</b></span>', ['class' => 'btn btn-secondary','onclick'=>"procesar('".$nom_tbl."');", 'id' => 'btn-procesar-'.$nom_tbl]);
                            }
                        ]                            
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
            <h1><?= Html::encode('Proceso') ?></h1>
            <div id="resultado_proceso"></div>
    </div>
</div>
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">DATOS PARA PROCESAR MIGRACION</h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form">
                    <input type="hidden" id="nom_tbl" name="nom_tbl" />
                    <div class="form-group">
                        <label for="inputName">LIMIT</label>
                        <input type="text" class="form-control" id="limit" name="limit" placeholder="Ingrese el limit"/>
                    </div>
                    <div class="form-group">
                        <label for="inputName">OFFSET</label>
                        <input type="text" class="form-control" id="offset" name="offset" placeholder="Ingrese el offset"/>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary submitBtn" onclick="procesar_data()">Procesar datos</button>
            </div>
        </div>
    </div>
</div>
